<?php

namespace App\Form;

use App\Entity\CoOwnerShip\Building;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class BuildingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Nom"
            ])
            ->add('buildingNumber', IntegerType::class, [
                'label' => "Numéro du bâtiment",
                'required' => false
            ])
            ->add('stair', TextType::class, [
                'label' => "Montée",
                'required' => false
            ])
            ->add('tantiemes', IntegerType::class, [
                'label' => "Tantièmes"
            ])
            ->add('tantiemesGarageBox', IntegerType::class, [
                'label' => "Tantième Garage Box",
                'required' => false
            ])
            ->add('tantiemesShop', IntegerType::class, [
                'label' => "Tantièmes Commerce",
                'required' => false
            ])
            ->add('others', IntegerType::class, [
                'label' => "Autres",
                'required' => false
            ])
            ->add('tantiemesHeating', IntegerType::class, [
                'label' => "Tantièmes Chauffage",
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Building::class,
        ]);
    }
}
