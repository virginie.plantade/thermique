<?php

namespace App\Form;

use App\Entity\Scenario\WorkHasScenario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Scenario\Work;
use App\Form\WorkType;
use App\Repository\WorkRepository;

class WorkHasScenarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('work', EntityType::class, [
                'class' => Work::class,
                'choice_label' => function(Work $work) {
                  return $work->getName();
                },
                'query_builder' => function(WorkRepository $wr) use($options) {
                  return $wr->findWorkNameByCoOwnership($options['coOwnership']);
                }
              ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['coOwnership']);
        $resolver->setDefaults([
            'data_class' => WorkHasScenario::class,
        ]);
    }
}
