<?php

namespace App\Form\Questionnaire;

use App\Entity\CoOwnerShip\Owner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class QuestIFSubventionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gotAnah', CheckboxType::class, [
                'label' => 'Cochez cette case si vous avez bénéficié de la subvention ANAH il y a moins de 5 ans.',
                'required' => false,
                'attr' => [
                    'class' => 'switch_1'
                ]
            ])
            ->add('subAnahAmount', IntegerType::class, [
                'required' => false,
                'label' => 'Si vous avez coché la case ci-dessus, indiquez le montant de la subvention:',
                'attr' => [
                    'placeholder' => 'ex : 100']
            ])
            ->add('gotTaxCredit', CheckboxType::class, [
                'label' => 'Cochez cette case si vous avez bénéficié d\'un crédit d\'impôt il y a moins de 5 ans.',
                'required' => false,
                'attr' => [
                    'class' => 'switch_1'
                ]
            ])
            ->add('taxCreditAmount',  IntegerType::class, [
                'required' => false,
                'label' => 'Si vous avez coché la case ci-dessus, indiquez le montant du crédit d\'impôt:',
                'attr' => [
                    'placeholder' => 'ex : 100']
            ])
            ->add('gotEcoCredit', CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'switch_1'
                ]
            ])
            ->add('gotPTZ', CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'switch_1'
                ]
            ])
            ->add('Sauvegarder', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
       }
}
