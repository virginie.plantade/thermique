<?php

namespace App\Form\Grant;

use App\Entity\Grant\Grant;
use App\Entity\CoOwnerShip\CoOwnership;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Document\DocumentCategory;

class GrantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,  array(
                "label"=>"Grant.name"))
            ->add('position')
            ->add('setting')
            ->add('population', ChoiceType::class, [
                'choices' => DocumentCategory::$populationsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez un type de destinataires'],
                'label' => 'Destinataires',
                'required' => false,
                'empty_data' =>"0",
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Grant::class,
        ]);
    }
}
