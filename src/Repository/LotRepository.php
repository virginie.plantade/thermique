<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Lot;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\CoOwnerShip\Building;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Lot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lot[]    findAll()
 * @method Lot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Lot::class);
    }

    protected function getQbLot(CoOwnership $coOwnership, ?string $number)
    {
        return $this->createQueryBuilder('l')
            ->leftJoin('l.building', 'b')
            ->where('l.number = :number')
            ->setParameter('number', $number)
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership);
    }

    public function isLotExist(CoOwnership $coOwnership, ?string $number) {
      $qb = $this->getQbLot($coOwnership, $number)->select('count(l.number)');

      $result = $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);

      return $result > 0;
    }

    public function getLotByNumber(CoOwnership $coOwnership, ?string $number) {
        $qb = $this->getQbLot($coOwnership, $number);
        $res = $qb->getQuery()
            ->getResult();

        if(count($res) > 0) {
            return $res['0'];
        }
        return null;
    }

    public function findLotsByCoOwnership(CoOwnership $coOwnership) {
      $qb = $this->createQueryBuilder('l')
                 ->addSelect('l')
                 ->leftJoin('l.building', 'b')
                 ->andWhere('b.coOwnership = :coOwnership')
                 ->setParameter('coOwnership', $coOwnership)
                 ->getQuery()
                 ->getResult();

      return $qb;
    }

    public function findLotByBuilding(Building $building) {
      $qb = $this->createQueryBuilder('l')
            ->andWhere('l.building = :building')
            ->setParameter('building', $building)
            ->getQuery();

      return $qb;
    }

    public function findLotByOwner(Owner $owner) {
      $qb = $this->createQueryBuilder('l')
            ->addSelect('o')
            ->leftJoin('l.owners', 'o')
            ->andWhere('o.id = :owner_id')
            ->setParameter('owner_id', $owner->getId())
            ->getQuery()
            ->getOneOrNullResult();

      return $qb;
    }

    public function findLotsByOwner(Owner $owner) {
      $qb = $this->createQueryBuilder('l')
            ->addSelect('o')
            ->leftJoin('l.owners', 'o')
            ->andWhere('o.id = :owner_id')
            ->setParameter('owner_id', $owner->getId())
            ->getQuery()
            ->getResult();

      return $qb;
    }

    // /**
    //  * @return Lot[] Returns an array of Lot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lot
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
