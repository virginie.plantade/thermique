<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CoOwnerShip\CoOwnership;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\User\User;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index()
    {
        return $this->render('admin/admin.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/projets/{maxItemPerPage}", name="coOwnerships_index")
     */
    public function projectList(PaginatorInterface $paginator, Request $request, int $maxItemPerPage = 20)
    {
      $repo = $this->getDoctrine()->getRepository(CoOwnership::class);
      $projectNotArchived = $repo->findNotArchivedValues();
      $projects = $paginator->paginate(
          $projectNotArchived->getQuery(),
          $request->query->getInt('page', 1),
          $maxItemPerPage
      );

      return $this->render('coOwnership/index.html.twig', [
          'projects' => $projects,
          'archived' => false,
          '_itemNum' => 0
      ]);
    }

    /**
     * @Route("/archives", name="coOwnership_archives")
     */
    public function projectArchive(PaginatorInterface $paginator, Request $request)
    {
      $repo = $this->getDoctrine()->getRepository(CoOwnership::class);
      $projectArchived = $repo->findArchivedValues();
      $projects = $paginator->paginate(
          $projectArchived->getQuery(),
          $request->query->getInt('page', 1),
          20
      );

        return $this->render('coOwnership/index.html.twig', [
            'projects' => $projects,
            'archived' => true,
        ]);
    }

    /**
     * @Route("/archive/{id}/ajouter", name="coOwnership_archive_add")
     */
    public function projectAddArchive(int $id)
    {
      $repo = $this->getDoctrine()->getRepository(CoOwnership::class);
      $project = $repo->findNotArchivedValueById($id);
      $project->setArchived(true);
      $this->getDoctrine()->getManager()->flush();

      return $this->redirectToRoute('coOwnership_archives');
    }

    /**
     * @Route("/archive/{id}/enlever", name="coOwnership_archive_reset")
     */
    public function projectResetArchive(int $id)
    {
      $repo = $this->getDoctrine()->getRepository(CoOwnership::class);
      $project = $repo->findArchivedValueById($id);
      $project->setArchived(false);
      $this->getDoctrine()->getManager()->flush();

      return $this->redirectToRoute('coOwnership_archives');
    }
}
