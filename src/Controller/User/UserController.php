<?php

namespace App\Controller\User;

use App\Entity\User\User;
use App\Entity\CoOwnerShip\Owner;
use App\Form\User\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/parameter/user")
 */
class UserController extends AbstractController
{
    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }
    /**
     * @Route("/", name="users_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('object/index.html.twig', [
            'users' => $userRepository->findUsers()->getResult(),
            'object' => 'user',
            'reference' => null
        ]);
    }

    /**
     * @Route("/new/{owner_id}", name="user_new", methods={"GET","POST"}, defaults={"owner_id" = null})
     * @ParamConverter("owner", class=Owner::class, options={"mapping": {"owner_id" = "id"}})
     */
    public function new(Request $request, Owner $owner = null)
    {
        $user = new User();
        $user->setOwner($owner);
        if($owner) {
            $user->setEmail($owner->getMail());
            $user->setEmailCanonical($owner->getMail());
        }
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Compte créé');

            if($user->getOwner()) {
                return $this->redirectToRoute('owner_show', [
                        'referenceId' => $user->getOwner()->getCoOwnership()->getId(),
                        'id' => $user->getOwner()->getId()]
                );
            } else {
                return $this->redirectToRoute('users_index');
            }
        }

        return $this->render('object/new.html.twig', [
            'object' => 'user',
            'reference' => null,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @ParamConverter("user", class=User::class, options={"mapping": {"id" = "id"}})
     */
    public function show(UserRepository $userRepository, User $user)
    {
//        $user = $userRepository->find($id);
        return $this->render('object/view.html.twig', [
            'object' => 'user',
            'entity' => $user,
            'reference' => null,
            'deleteLabel' => $user->isEnabled() ? 'Désactivé' : 'Activé'
                    ]);
    }
    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
//            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->flush();
            $this->userManager->updateUser($user, true);
            $this->addFlash('success', 'Compte modifé');
            if($user->getOwner()) {
//                dump($user->getOwner()->getCoOwnership()->getId());die;
                return $this->redirectToRoute('owner_show', [
                    'referenceId' => $user->getOwner()->getCoOwnership()->getId(),
                    'id' => $user->getOwner()->getId()]
                );
            } else {
                return $this->redirectToRoute('users_index');
            }
        }

        return $this->render('object/edit.html.twig', [
            'entity' => $user,
            'object' => 'user',
            'reference' => null,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->userUpdate($id);
    }

    /**
     * @Route("/{id}/update", name="user_update")
     */
    public function userUpdate($id) {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $user->setEnabled(!$user->isEnabled());
        $entityManager->flush();
        if($user->isEnabled()) {
            $this->addFlash('success', 'Compte activé');
        } else {
            $this->addFlash('success', 'Compte désactivé');
        }
        return $this->redirectToRoute('users_index');
    }
}