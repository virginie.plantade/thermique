<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User\User;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
            return $this->redirectRole();

    }

    /**
     * @Route("/redirect", name="redirect_choice")
     */
    public function redirectRole()
    {
      $user = $this->getUser();
      if(!$user) {
          return $this->redirectToRoute('fos_user_security_login');
      }

      if($this->isGranted('ROLE_ADMIN') or $this->isGranted('ROLE_MANAGER')) {
        return $this->redirectToRoute('coOwnerships_index');
      } else {

//        if(!$user->getFirstConnection()) {
//          $user->setFirstConnection(true);
//          $this->getDoctrine()->getManager()->flush();
//          return $this->redirectToRoute('fos_user_change_password');
//        }

        return $this->redirectToRoute('user', [
          'user_id' => $user->getId(),
        ]);
      }
    }

}
