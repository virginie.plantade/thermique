<?php

namespace App\Controller\Document;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\Document\UploadedFile;
use App\Entity\Grant\Grant;
use App\Form\Grant\GrantType;
use App\Form\Grant\AddGrantType;
use App\Form\Grant\TestGrantType;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\Document\GenerateDocument;
use Symfony\Component\HttpKernel\KernelInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;



/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class DocumentController extends AbstractController
{
    private $appKernel;
    private $generateDocument;
    private $uploaderHelper;
    private $uploads_dir;

    public function __construct($uploads_dir, KernelInterface $appKernel, GenerateDocument $generateDocument, UploaderHelper $uploaderHelper)
    {
        $this->appKernel = $appKernel;
        $this->generateDocument = $generateDocument;
        $this->uploaderHelper = $uploaderHelper;
        $this->uploads_dir = $uploads_dir;
    }

    /**
     * @Route({
     *  "en": "/documents",
     *  "fr": "/documents"
     * }, name="documents_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership, int $maxItemsPerPage = 15): Response
    {
        $grants = array();
//        $repository = $this->getDoctrine()->getRepository(Grant::class);
//        $grants = $paginator->paginate(
//            $coOwnership->getGrants(),
//            $request->query->getInt('page', 1),
//            $maxItemsPerPage
//        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'grants' => $grants,
            'object' => 'grant'
        ]);
    }

    protected function testOwner(Owner $owner, CoOwnership $coOwnership, UploadedFile $file = null)
    {
        $origin = null;
        $docResult = null;
//        $results = array();
//        $repository = $this->getDoctrine()->getRepository(Scenario::class);
//        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        if($file) {
            $origin = $this->uploaderHelper->asset($file, 'file');
            $filename = sprintf("%s-%s.%s", pathinfo($origin, PATHINFO_FILENAME), $owner, pathinfo($origin, PATHINFO_EXTENSION));
            $docResult = pathinfo($origin, PATHINFO_DIRNAME)."/".$filename;
        }
        $documentPath = $this->appKernel->getProjectDir(). '/uploads/coOwnership/' .$coOwnership->getBusinessNumber() .'/';

        return $this->generateDocument->generateDocument($owner, $documentPath, $origin, $docResult);
    }

    /**
     * @Route({
     *  "en": "/documents/test/{owner_id}",
     *  "fr": "/documents/test/{owner_id}"
     * }, name="document_test", defaults={"owner_id" = null})
     * @ParamConverter("owner", class=Owner::class, options={"mapping": {"owner_id": "id"}})
     */
    public function test(Request $request, Owner $owner = null, CoOwnership $coOwnership)
    {

        $res = $this->testOwner($owner, $coOwnership);

//        $repository = $this->getDoctrine()->getRepository(Scenario::class);
//        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        return $this->render('coOwnership/document/test.html.twig', [
            'coOwnership' => $coOwnership,
            'owner' => $owner,
            'result' => $res
        ]);

    }

    /**
     * @Route({
     *  "en": "/documents/{file_id}/generate",
     *  "fr": "/documents/{file_id}/generation",
     * }, name="document_generate")
     * @ParamConverter("file", class=UploadedFile::class, options={"mapping": {"file_id": "id"}})
     */
    public function generate(Request $request, CoOwnership $coOwnership, UploadedFile $file)
    {
        $count = 0;
        $category = null;
        if($file->getCategory()) {
            $category = $file->getCategory()->getChildCategory();
        }
        $repository = $this->getDoctrine()->getRepository(UploadedFile::class);
        $entityManager = $this->getDoctrine()->getManager();
        $now = new \DateTime();

//        dump($coOwnership->getCompleteOwners());die;
        //dump($coOwnership->getCompleteOwners($file->getCategory()));die;
        foreach($coOwnership->getCompleteOwners($file->getCategory()) as $owner) {
            $res = $this->testOwner($owner, $coOwnership, $file);

            if($res) {
                $fileName = basename($res);
                $child = $repository->findOneBy(array('owner' => $owner, 'parentFile' => $file));
                if(!$child) {
                    $child = new UploadedFile();
                    $child->setCoOwnership($coOwnership);
                    $child->setOwner($owner);
                    $child->setCategory($category);
                    $child->setParentFile($file);
                    $entityManager->persist($child);
                }
                $child->setUpdatedAt($now);
                $child->setTitle(sprintf("simulation %s", $owner));
                $child->setFileName($fileName);
                $entityManager->flush();
                $count++;
            }
        }

        $this->addFlash('success', $count.' documents générés');

        return $this->redirectToRoute('uploadedFiles_index', ['referenceId' => $coOwnership->getId()]);

    }

    /**
     * @Route({
     *  "en": "/documents/{file_id}/generate-pdf",
     *  "fr": "/documents/{file_id}/generation-pdf",
     * }, name="document_generate_pdf")
     * @ParamConverter("file", class=UploadedFile::class, options={"mapping": {"file_id": "id"}})
     */
    public function generatePdf(Request $request, CoOwnership $coOwnership, UploadedFile $file)
    {
        $count = 0;
        $repository = $this->getDoctrine()->getRepository(UploadedFile::class);
        $files = $repository->findBy(array('parentFile' => $file));
        $entityManager = $this->getDoctrine()->getManager();
        $now = new \DateTime();

//        dump($coOwnership->getCompleteOwners());die;
        foreach($files as $child) {
            $origin = $this->uploaderHelper->asset($child, 'file');
            $res = $this->generateDocument->convert2pdf($origin);

            if($res) {
                $fileName = basename($res);
                $child->setPdfFileName($fileName);
                $child->setPdfUpdatedAt($now);
                $entityManager->flush();
                $count++;
            }
        }

        $this->addFlash('success', $count.' documents pdf générés');

        return $this->redirectToRoute('uploadedFiles_index', ['referenceId' => $coOwnership->getId()]);

    }

    /**
     * @Route({
     *  "en": "/documents/zipall",
     *  "fr": "/documents/zipall",
     * }, name="zip_all")
     */
    public function zipall(Request $request, CoOwnership $coOwnership)
    {
        $folder = $this->uploads_dir.'/coOwnership/'.$coOwnership->getBusinessNumber();

        $zip = new \ZipArchive();
        $zipName = '../uploads/Documents-'.time().$coOwnership->getBusinessNumber().".zip";
        $zip->open($zipName,  \ZipArchive::CREATE);
//        foreach ($files as $f) {
//            $zip->addFromString(basename($f),  file_get_contents($f));
//        }
        if ($handle = opendir($folder)) {

            while (false !== ($entry = readdir($handle))) {

                if ($entry != "." && $entry != "..") {

//                    dump($entry);die;
                    $zip->addFromString(basename($entry),  file_get_contents($folder.'/'.$entry));
                }
            }

            closedir($handle);
        }

        $zip->close();
        $response = new Response();

//        dump($zipName);die;
        $filename = sprintf("%s.zip", pathinfo($zipName, PATHINFO_FILENAME));
//        $filename = "/home/stephane/gitwork/src/thermique/uploads/Documents-157589904418083-38.zip";
        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', mime_content_type($zipName));
        $response->headers->set('Content-Disposition', 'inline;filename="' .$filename.'"');
//        return $response;
//        $response->setContent(readfile($zipName));
//        $response->headers->set('Content-Type', 'application/zip');
//        $response->header('Content-disposition: attachment; filename="'.$zipName.'"');
//        $response->header('Content-Length: ' . filesize($zipName));
//        $response->readfile($zipName);
        return $response;
    }
}
