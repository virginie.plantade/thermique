<?php

namespace App\Service\Document;

use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\Scenario\WorkHasBuilding;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpWord\Reader\RTF\Document;
use PhpOffice\PhpWord\Shared\ZipArchive;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\PhpWord;
use \DOMDocument;
use App\Service\GenerateSubsidies;
use App\Entity\Scenario\Scenario;
use App\Repository\ScenarioRepository;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;



class GenerateDocument

{
    const FIRST = 1500;
    const OTHER = 1000;

    public static $fontStyle = array('color' => 'ffffff', 'bold' => true);
    public static $greenStyle = array('color' => '00b050', 'bold' => true);

    private $em;
    private $generateSubsidies;
    private $scenarioRepository;
    private $number;
    private $images;
    private $pdfdir;

    public function __construct(EntityManagerInterface $entityManager, $images, $pdfdir, GenerateSubsidies $generateSubsidies, ScenarioRepository $scenarioRepository)
    {
        $this->em = $entityManager;
        $this->generateSubsidies = $generateSubsidies;
        $this->scenarioRepository = $scenarioRepository;
        $this->number = 0;
        $this->images = $images;
        $this->pdfdir = $pdfdir;
    }

    protected function addScenarios(Table $table, Owner $owner, CoOwnership $coOwnership)
    {
//        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $this->scenarioRepository->findScenariosByCoOwnership($coOwnership)->getResult();
//        $jsonSchemaPath = $this->appKernel->getProjectDir(). '/assets/';
        $totals = [];
        $i = 0;
        foreach ($scenarios as $scenario) {
            $totals[$i++] = 0;
//            $i++;
        }
//        dump($totals);die;
        foreach($coOwnership->getValidGrants($owner) as $grant) {
            $style = $this->getRow();
            $table->addRow();
            $table->addCell(self::FIRST, $style)->addText($grant->__toString(), $style, array('align' => 'left'));
            $res = [];
            $i = 0;
            foreach ($scenarios as $scenario) {
                $results = $this->generateSubsidies->generateSubsidies($owner, $scenario, $grant);
                $text = sprintf("%s €", $results->getData());
                $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'right'));
                $totals[$i++] += $results->getData();
            }
        }
        $style = $this->getRow(true, true);
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Total des aides mobilisables', $style, array('align' => 'left'));
        foreach($totals as $total) {
            $text = sprintf("%s €", $total);
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'right'));
        }
        return $totals;
    }

    protected function replaceTags(Owner $owner, $origin, $docResult)
    {
        $res = '';

        copy($origin,$docResult);

        $phpword = new \PhpOffice\PhpWord\TemplateProcessor($origin);

        $name = sprintf("%s", $owner);
        $phpword->setValue('{name}',$name);
        $phpword->setValue('{lastname}',$owner->getLastname());
        $phpword->setValue('{firstname}',$owner->getFirstname());
        $phpword->setValue('{gender}',$owner->getGender());
        $phpword->setValue('{address}',$owner->getGlobalAddress());

        $phpword->saveAs($docResult);

        return $res;
    }

    protected function addBgColor(&$style, $color)
    {
        $style['bgColor'] = $color;
        $style['borderColor'] = $color;
        $style['borderBottomSize'] = 0;
        $style['borderTopSize'] = 0;
        $style['borderLeftSize'] = 0;
        $style['borderRightSize'] = 0;
    }

    protected function addHeaderStyle(&$style)
    {
        $this->addBgColor($style, "9dc3e6");
        $style['color'] = 'ffffff';
        $style['bold'] = true;
    }

    protected function getRow($bold = false, $green = false)
    {
        $style = array();
        if($green) {
            $style['color'] = '00b050';
        } else {
            $style['color'] = '000000';
        }
        $style['bold'] = $bold;

        $this->addBgColor($style, ($this->number % 2) === 0 ?  'ffffff' : "deebf7");
        $this->number++;
        return $style;
    }

    protected function adjustEnergyColor(&$style, $text)
    {
        switch($text) {
            case 'A++':
            case 'A+':
            case 'A': $style['bgColor'] = '33a357';break;
            case 'B': $style['bgColor'] = '79b752';break;
            case 'C': $style['bgColor'] = 'c3d545';break;
            case 'D': $style['bgColor'] = 'fff12c';break;
            case 'E': $style['bgColor'] = 'edb731';break;
            case 'F': $style['bgColor'] = 'd66f2c';break;
            case 'G': $style['bgColor'] = 'cc232a';break;
            default: break;
        }
        $style['color'] = '000000';
    }

    protected function createTableDoc(Owner $owner, CoOwnership $coOwnership, $docResult)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        /* Note: any element you append to a document must reside inside of a Section. */

// Adding an empty Section to the document...
        $section = $phpWord->addSection();
        $table = $section->addTable(array('borderSize' => -5, 'borderColor' => '9dc3e6', 'width' => 9000, 'unit' => TblWidth::TWIP));
        $headerStyle = array();
        $fontStyle = array('color' => 'ffffff', 'bold' => true);
        $greenStyle = array('color' => '00b050', 'bold' => true);
        $this->addHeaderStyle($headerStyle);
//        dump($headerStyle);die;

        $scenarios = $this->scenarioRepository->findScenariosByCoOwnership($coOwnership)->getResult();

        $table->addRow();
        $table->addCell(self::FIRST, $headerStyle);
        foreach ($scenarios as $scenario) {
            $table->addCell(self::OTHER, $headerStyle)->addText($scenario->__toString(), self::$fontStyle, array('align' => 'center', 'spaceAfter' => 100,'spaceBefore' => 100));
        }

        $style = $this->getRow();
        $table->addRow();

        $table->addCell(self::FIRST, $style)->addText('Gain énergétique global copropriété :', $style, array('align' => 'left'));
        foreach ($scenarios as $scenario) {
            $text = sprintf("%d %%", floor($scenario->getEnergyGain()));
//            $style2 = [];
//            $style2['alignment'] = 'center';
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'center', 'spaceAfter' => 100,'spaceBefore' => 100));
        }

//        $myImage = $this->images."star0.jpg";
//        dump($this->images);
//        dump($myImage);die;
        $style = $this->getRow();
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Confort', $style, array('align' => 'left', 'spaceAfter' => 100,'spaceBefore' => 100));
        foreach ($scenarios as $scenario) {
            $text = sprintf("%d / 5", $scenario->getConfort());
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'center', 'spaceAfter' => 100,'spaceBefore' => 100));
        }

        // inheritance gain
        $style = $this->getRow();
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Valorisation du patrimoine', $style, array('align' => 'left', 'spaceAfter' => 100,'spaceBefore' => 100));
        foreach ($scenarios as $scenario) {
            $text = sprintf("%d / 5", $scenario->getInheritanceGain());
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'center', 'spaceAfter' => 100,'spaceBefore' => 100));
        }

        // origin tag
        $style = $this->getRow();
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Etiquette énergétique d\'origine', $style, array('align' => 'left'));
        foreach ($scenarios as $scenario) {
            $text = sprintf("%s", $scenario->getCarbonIndexTag());
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'center', 'spaceAfter' => 100,'spaceBefore' => 100));
        }

        // end tag
        $style = $this->getRow(true, true);
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Etiquette énergétique atteinte', $style, array('align' => 'left', 'spaceAfter' => 100,'spaceBefore' => 100));

        foreach ($scenarios as $scenario) {
            $text = sprintf("%s", $scenario->getEnergyFinalTag());
            $this->adjustEnergyColor($style, $text);

            $cell = $table->addCell(self::OTHER, $style);
            $cell->addText($text, $style, array('align' => 'center', 'spaceAfter' => 100,'spaceBefore' => 100));
//            $cell->setVAlign('center');
//            dump($style);die;
        }

        // investment ttc
        $cost = [];
        $style = $this->getRow(true);
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Prix de l\'opération pour vous avant aides', $style, array('align' => 'left'));
        foreach ($scenarios as $scenario) {
            $cost[] = 100 * floor($owner->getInvestment($scenario)/100);
            $text = sprintf("%d €", 100 * floor($owner->getInvestment($scenario)/100));
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'right'));
        }

        $subsidies = $this->addScenarios($table, $owner, $coOwnership);

        // subsidies percent
        $this->number--;
        $style = $this->getRow();
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('(% aides par rapport au prix de l\'opération)', $style, array('align' => 'right'));
        $i = 0;
        foreach ($subsidies as $subsidy) {
            $text = sprintf("(%d %%)", $cost[$i] > 0 ? floor($subsidy * 100 / $cost[$i++]) : 0);
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'right'));
        }

        // works fund
        $workfund = $coOwnership->getWorksFund() * $owner->getFirstLot()->getTantiemesApartment()/$coOwnership->getTantiemesTotal();
        $style = $this->getRow();
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Fond de travaux', $style, array('align' => 'left'));
        foreach ($scenarios as $scenario) {
            $text = sprintf("%d €", $workfund);
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'right'));
        }

        // total
        $style = $this->getRow(true);
        $table->addRow();
        $table->addCell(self::FIRST, $style)->addText('Reste à charge toute aide déduite', $style, array('align' => 'left'));
        $i = 0;
        foreach ($subsidies as $subsidy) {
            $text = sprintf("%d €", $cost[$i++] - $subsidy - floor($workfund));
            $table->addCell(self::OTHER, $style)->addText($text, $style, array('align' => 'right'));
        }

        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save($docResult);

        return 'success';
    }

    protected function mergeDocs(Owner $owner, $templateFile, $generatedFile, $targetFile)
    {

// copy template to target
        $this->replaceTags($owner, $templateFile, $targetFile);
//        copy($templateFile, $targetFile);

// open target
        $targetZip = new \ZipArchive();
        $targetZip->open($targetFile);
        $targetDocument = $targetZip->getFromName('word/document.xml');
        $targetDom      = new DOMDocument();
        $targetDom->loadXML($targetDocument);
        $targetXPath = new \DOMXPath($targetDom);
        $targetXPath->registerNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

// open source
        $sourceZip = new \ZipArchive();
        $sourceZip->open($generatedFile);
        $sourceDocument = $sourceZip->getFromName('word/document.xml');
        $sourceDom      = new DOMDocument();
        $sourceDom->loadXML($sourceDocument);
        $sourceXPath = new \DOMXPath($sourceDom);
        $targetXPath->registerNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

        /** @var DOMNode $replacementMarkerNode node containing the replacement marker $CONTENT$ */
        $replacementMarkerNodes = $targetXPath->query('//w:p[contains(translate(normalize-space(), " ", ""),"{scenarios}")]');

// insert source nodes before the replacement marker
        $sourceNodes = $sourceXPath->query('//w:document/w:body/*[not(self::w:sectPr)]');

//        dump($sourceNodes);die;
        foreach ($replacementMarkerNodes as $replacementMarkerNode) {
            foreach ($sourceNodes as $sourceNode) {
//                dump($sourceNode);die;
                $imported = $replacementMarkerNode->ownerDocument->importNode($sourceNode, true);
                $inserted = $replacementMarkerNode->parentNode->insertBefore($imported, $replacementMarkerNode);

            }
        }
        foreach ($replacementMarkerNodes as $replacementMarkerNode) {
            $replacementMarkerNode->parentNode->removeChild($replacementMarkerNode);
        }

// remove $replacementMarkerNode from the target DOM


// save target
        $targetZip->addFromString('word/document.xml', $targetDom->saveXML());
        $targetZip->close();
    }

    public function addImages($temp)
{
    $targetZip = new \ZipArchive();
    $targetZip->open($temp);
//    $targetDocument = $targetZip->getFromName('word/document.xml');
//    $targetDom      = new DOMDocument();
//    $targetDom->loadXML($targetDocument);
//    $targetXPath = new \DOMXPath($targetDom);
//    $targetXPath->registerNamespace("w12", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
    $source = $this->images."star0.png";
    $targetZip->addFile($source, 'word/media/' . 'star0.png');
//    $targetZip->addFromString('word/document.xml', $targetDom->saveXML());
    $targetZip->close();
}

    public function generateDocument(Owner $owner, $documentPath, $origin = null, $docResult = null)
    {
        $res = 0;
        // data
        $lot = $owner->getLots()[0];
        $building = $lot->getBuilding();
        $coOwnership = $building->getCoOwnership();
//        $workHasBuildings = $this->em->getRepository(WorkHasBuilding::class)->findByBuildingScenario($building, $scenario)->getResult();

        if(!$origin) {
            $origin = realpath($documentPath . 'simulation.docx');
        }
        $temp = $documentPath . 'tmp-'.$owner->getId().'.docx';
        $this->createTableDoc($owner, $coOwnership, $temp);
//        $this->addImages($temp);

        if(!$docResult) {
            $docResult = $documentPath . 'simulation-' . $owner->getId() . '.docx';
        }
        $this->mergeDocs($owner, $origin, $temp, $docResult);
        unlink($temp);
//        dump($origin);dump($docResult);die;

//        $res = $this->replaceTags($owner, $origin, $docResult);

        return $docResult;
    }

    public function convert2pdf($origin)
    {
        $filename = sprintf("%s.pdf", pathinfo($origin, PATHINFO_FILENAME));
        $docResult = pathinfo($origin, PATHINFO_DIRNAME)."/".$filename;

// Set PDF renderer.
// Make sure you have `tecnickcom/tcpdf` in your composer dependencies.
        if(!Settings::setPdfRendererName(Settings::PDF_RENDERER_TCPDF)) {
//            dump("pb name");die;
        }
// Path to directory with tcpdf.php file.
// Rigth now `TCPDF` writer is depreacted. Consider to use `DomPDF` or `MPDF` instead.
        $pdfdir = $this->pdfdir.'vendor/tecnick.com/tcpdf/';
        dump($pdfdir);
        if(!Settings::setPdfRendererPath($pdfdir)) {
//            dump('error path');die;
        }

//        dump($docResult);die;
        $phpWord = IOFactory::load($origin, 'Word2007');
        $phpWord->save($docResult, 'PDF');
    }

}
