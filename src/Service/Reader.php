<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Ods;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class Reader {

  public function readFile($filename) {
    $extension = pathinfo($filename, PATHINFO_EXTENSION);

//    dump($filename);die;
    switch ($extension) {
      case 'ods':
        $reader = new Ods();
        break;
      case 'xlsx':
        $reader = new Xlsx();
        break;
      case 'xls':
        $reader = new Xls();
        break;
      case 'csv':
        $reader = new Csv();
        break;
      default:
        throw new \Exception('Invalid extension: '.$extension);
    }

    $reader->setReadDataOnly(true);
    return $reader->load($filename);
  }

  public function createDataFromSpreadsheet($spreadsheet, $type) {
      switch($type) {
          case 'scenario': $maxIndex = 0; break;
          default : $maxIndex = 0; break;
      }
    $data = [];
    foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
      foreach ($worksheet->getRowIterator() as $row) {
        $rowIndex = $row->getRowIndex();
        $data[$rowIndex] = [];
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false); // Loop over all cells, even if it is not set
        foreach ($cellIterator as $cell) {
          if($rowIndex > $maxIndex) {
            $data[$rowIndex][] = $cell->getCalculatedValue();
          }
        }
      }
    }

    return $data;
  }
}
