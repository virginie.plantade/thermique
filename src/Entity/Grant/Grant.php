<?php

namespace App\Entity\Grant;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\CoOwnerShip\CoOwnership;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Grant\GrantRepository")
 * @ORM\Table(name="subsidyProject")
 */
class Grant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("string")
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $setting;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $population;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     * 
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CoOwnerShip\CoOwnership", inversedBy="grants")
     */
    private $coOwnership;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSetting(): ?string
    {
        return $this->setting;
    }

    public function setSetting(string $setting): self
    {
        $this->setting = $setting;

        return $this;
    }

    public function getPopulation(): ?string
    {
        return $this->population;
    }

    public function setPopulation(string $population): self
    {
        $this->population = $population;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return CoOwnership
     */

    public function getCoOwnership(): ?CoOwnership
    {
        return $this->coOwnership;
    }

    public function setCoOwnership(?CoOwnership $coOwnership): self
    {
        $this->coOwnership = $coOwnership;

        return $this;
    }

    public function __toString() {
        return $this->name;
    }
}
