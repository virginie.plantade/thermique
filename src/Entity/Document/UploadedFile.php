<?php
namespace App\Entity\Document;

use App\Entity\User\User;
use App\Entity\CoOwnerShip\Owner as Owner;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\CoOwnerShip\CoOwnership;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="fichiers")
 * @ORM\Entity(repositoryClass="App\Repository\Document\UploadedFileRepository")
 * @Vich\Uploadable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UploadedFile
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="file", fileNameProperty="fileName")
     *
     * @var File
     * @Assert\NotBlank(message="Veuillez sélectionner un fichier", groups={"fileCreation"})
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $fileName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Document\DocumentCategory")
     * @Assert\NotBlank(message="Le type de fichier est obligatoire")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     * @Assert\NotBlank(message="Le libellé du fichier est obligatoire")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="attachedFiles")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * Ce champ est modifié à chaque fois pour lancer l'event listener de VichUploader
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $internalUpdatedAt;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     */
    protected $updatedBy;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $deletedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $pdfFileName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $pdfUpdatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\CoOwnerShip\CoOwnership", inversedBy="attachedFiles")
     */
    private $coOwnership;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\CoOwnerShip\Owner", inversedBy="attachedFiles")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="UploadedFile")
     */
    private $parentFile;

    public function __construct(CoOwnership $coOwnership = null, User $member = null)
    {
        $this->coOwnership = $coOwnership;
        $this->createdBy = $member;
        $this->createdAt = new \Datetime();
    }


    public function __toString()
    {
        return $this->title;
    }

    // Indique si le champ "catégorie" est de type string ou de type entité (comme pour les activité)
    public function hasEntityCategory()
    {
        return false;
    }


    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt;
        } else {
            return $this->createdAt;
        }
    }

    public function getExtension()
    {
        $path_parts = pathinfo($this->fileName);
        return $path_parts['extension'];

    }

    public function getOriginalFileName()
    {
        if (strpos($this->fileName, '_')!==false){
            return substr($this->getFileName(), strpos($this->fileName, '_')+1);
        } else {
            return $this->fileName;
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->internalUpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $pdfFileName
     */
    public function setPdfFileName($pdfFileName)
    {
        $this->pdfFileName = $pdfFileName;
    }

    /**
     * @return string
     */
    public function getPdfFileName()
    {
        return $this->pdfFileName;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return UploadedFile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set pdfUpdatedAt
     *
     * @param \DateTime $pdfUpdatedAt
     *
     * @return UploadedFile
     */
    public function setPdfUpdatedAt($pdfUpdatedAt)
    {
        $this->pdfUpdatedAt = $pdfUpdatedAt;

        return $this;
    }

    /**
     * Get pdfUpdatedAt
     *
     * @return \DateTime
     */
    public function getPdfUpdatedAt()
    {
        return $this->pdfUpdatedAt;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UploadedFile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \App\Entity\User\User $createdBy
     *
     * @return UploadedFile
     */
    public function setCreatedBy(\App\Entity\User\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \App\Entity\User\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \App\Entity\User\User $updatedBy
     *
     * @return UploadedFile
     */
    public function setUpdatedBy(\App\Entity\User\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \App\Entity\User\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return UploadedFile
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return DocumentCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return UploadedFile
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set internalUpdatedAt
     *
     * @param \DateTime $internalUpdatedAt
     *
     * @return UploadedFile
     */
    public function setInternalUpdatedAt($internalUpdatedAt)
    {
        $this->internalUpdatedAt = $internalUpdatedAt;

        return $this;
    }

    /**
     * Get internalUpdatedAt
     *
     * @return \DateTime
     */
    public function getInternalUpdatedAt()
    {
        return $this->internalUpdatedAt;
    }

    /**
     * Set coOwnership
     *
     * @param \App\Entity\CoOwnership\CoOwnership $coOwnership
     *
     * @return UploadedFile
     */
    public function setCoOwnership(\App\Entity\CoOwnership\CoOwnership $coOwnership = null)
    {
        $this->coOwnership = $coOwnership;

        return $this;
    }

    /**
     * Get coOwnership
     *
     * @return \App\Entity\CoOwnership\CoOwnership
     */
    public function getCoOwnership()
    {
        return $this->coOwnership;
    }

    /**
     * Set owner
     *
     * @param \App\Entity\CoOwnership\Owner $owner
     *
     * @return UploadedFile
     */
    public function setOwner(\App\Entity\CoOwnership\Owner $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \App\Entity\CoOwnership\Owner
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return UploadedFile
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parentFile
     *
     * @param \App\Entity\Document\UploadedFile $uploadedFile
     *
     * @return UploadedFile
     */
    public function setParentFile(\App\Entity\Document\UploadedFile $uploadedFile = null)
    {
        $this->parentFile = $uploadedFile;

        return $this;
    }

    /**
     * Get parentFile
     *
     * @return \App\Entity\Document\UploadedFile
     */
    public function getParentFile()
    {
        return $this->parentFile;
    }
}
