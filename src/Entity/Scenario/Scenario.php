<?php

namespace App\Entity\Scenario;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Export\Export as Export;
use App\Entity\CoOwnerShip\Building;
use App\Entity\Scenario\WorkHasBuilding;
use App\Entity\Scenario\Work;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScenarioRepository")
 */
class Scenario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $energyExpens;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $maintenanceExpens;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $largeExpens;

    /**
     *  @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer")
     */
    private $cep;

    /**
     *  @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer")
     */
    private $carbonIndex;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=4)
     */
    private $carbonIndexTag;

    /**
     *  @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer")
     */
    private $confort;

    /**
     *  @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     * @ORM\Column(type="integer")
     */
    private $inheritanceGain;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=4)
     */
    private $energyFinalTag;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     * @Assert\Range(min=0, max=100)
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     * @Groups({"grant"})
     */
    private $energyGain;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     * @Assert\Range(min=0, max=100)
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $environmentalGain;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=4)
     */
    private $environmentalGainTag;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $labelGain;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2, nullable=true)
     */
    private $timeReturn;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $investismentTtc;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $investismentHt;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $loadsEnergyGain;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $loadsGain;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scenario\WorkHasScenario", mappedBy="scenario", cascade={"persist"})
     */
    private $workHasScenarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Export\Export", mappedBy="scenario")
     */
    private $exports;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __construct()
    {
        $this->workHasScenarios = new ArrayCollection();
        $this->exports = new ArrayCollection();
        $this->setLargeExpens(0);
        $this->setConfort(0);
        $this->setEnergyGain(0);
        $this->setEnvironmentalGain(0);
        $this->setEnergyFinalTag('');
        $this->setMaintenanceExpens(0);
        $this->setLoadsEnergyGain(0);
        $this->setLoadsGain(0);
        $this->setCarbonIndexTag('');
        $this->setEnvironmentalGainTag('');
        $this->setLabelGain('');
    }

    public function __toString() {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnergyExpens(): ?string
    {
        return $this->energyExpens;
    }

    public function setEnergyExpens(string $energyExpens): self
    {
        $this->energyExpens = $energyExpens;

        return $this;
    }

    public function getMaintenanceExpens(): ?string
    {
        return $this->maintenanceExpens;
    }

    public function setMaintenanceExpens(string $maintenanceExpens): self
    {
        $this->maintenanceExpens = $maintenanceExpens;

        return $this;
    }

    public function getLargeExpens(): ?string
    {
        return $this->largeExpens;
    }

    public function setLargeExpens(string $largeExpens): self
    {
        $this->largeExpens = $largeExpens;

        return $this;
    }

    public function getCep(): ?int
    {
        return $this->cep;
    }

    public function setCep(int $cep): self
    {
        $this->cep = $cep;

        return $this;
    }

    public function getCarbonIndex(): ?int
    {
        return $this->carbonIndex;
    }

    public function setCarbonIndex(int $carbonIndex): self
    {
        $this->carbonIndex = $carbonIndex;

        return $this;
    }

    public function getCarbonIndexTag(): ?string
    {
        return $this->carbonIndexTag;
    }

    public function setCarbonIndexTag(string $carbonIndexTag): self
    {
        $this->carbonIndexTag = $carbonIndexTag;

        return $this;
    }

    public function getConfort(): ?int
    {
        return $this->confort;
    }

    public function setConfort(int $confort): self
    {
        $this->confort = $confort;

        return $this;
    }

    public function getInvestismentTtcByBuilding(Building $building): ?int
    {
        $total = 0;
        foreach ($this->workHasScenarios as $workHasScenario) {
            foreach ($workHasScenario->getWork()->getWorkHasBuildings() as $workHasBuilding) {
                if($workHasBuilding->getBuilding() == $building) {
                    $total += $workHasBuilding->getTotalCostTtc();
                }
            }
        }
        return $total;
    }

    public function getInheritanceGain(): ?int
    {
        return $this->inheritanceGain;
    }

    public function setInheritanceGain(int $inheritanceGain): self
    {
        $this->inheritanceGain = $inheritanceGain;

        return $this;
    }

    public function getEnergyFinalTag(): ?string
    {
        return $this->energyFinalTag;
    }

    public function setEnergyFinalTag(string $energyFinalTag): self
    {
        $this->energyFinalTag = $energyFinalTag;

        return $this;
    }

    public function getEnergyGain(): ?string
    {
        return $this->energyGain;
    }

    public function setEnergyGain(string $energyGain): self
    {
        $this->energyGain = $energyGain;

        return $this;
    }

    public function getEnvironmentalGain(): ?string
    {
        return $this->environmentalGain;
    }

    public function setEnvironmentalGain(string $environmentalGain): self
    {
        $this->environmentalGain = $environmentalGain;

        return $this;
    }

    public function getEnvironmentalGainTag(): ?string
    {
        return $this->environmentalGainTag;
    }

    public function setEnvironmentalGainTag(string $environmentalGainTag): self
    {
        $this->environmentalGainTag = $environmentalGainTag;

        return $this;
    }

    public function getLabelGain(): ?string
    {
        return $this->labelGain;
    }

    public function setLabelGain(string $labelGain): self
    {
        $this->labelGain = $labelGain;

        return $this;
    }

    public function getTimeReturn(): ?string
    {
        return $this->timeReturn;
    }

    public function setTimeReturn(string $timeReturn): self
    {
        $this->timeReturn = $timeReturn;

        return $this;
    }

    public function getInvestismentTtc(): ?string
    {
        return $this->investismentTtc;
    }

    public function setInvestismentTtc(string $investismentTtc): self
    {
        $this->investismentTtc = $investismentTtc;

        return $this;
    }

    public function getInvestismentHt(): ?string
    {
        return $this->investismentHt;
    }

    public function setInvestismentHt(string $investismentHt): self
    {
        $this->investismentHt = $investismentHt;

        return $this;
    }

    public function getLoadsEnergyGain(): ?string
    {
        return $this->loadsEnergyGain;
    }

    public function setLoadsEnergyGain(string $loadsEnergyGain): self
    {
        $this->loadsEnergyGain = $loadsEnergyGain;

        return $this;
    }

    public function getLoadsGain(): ?string
    {
        return $this->loadsGain;
    }

    public function setLoadsGain(string $loadsGain): self
    {
        $this->loadsGain = $loadsGain;

        return $this;
    }

    /**
     * @return Collection|WorkHasScenario[]
     */
    public function getWorkHasScenarios(): Collection
    {
        return $this->workHasScenarios;
    }

    public function addWorkHasScenario(WorkHasScenario $workHasScenario): self
    {
        if (!$this->workHasScenarios->contains($workHasScenario)) {
            $this->workHasScenarios[] = $workHasScenario;
            $workHasScenario->setScenario($this);
        }

        return $this;
    }

    public function removeWorkHasScenario(WorkHasScenario $workHasScenario): self
    {
        if ($this->workHasScenarios->contains($workHasScenario)) {
            $this->workHasScenarios->removeElement($workHasScenario);
            // set the owning side to null (unless already changed)
            if ($workHasScenario->getScenario() === $this) {
                $workHasScenario->setScenario(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Export[]
     */
    public function getExports(): Collection
    {
        return $this->exports;
    }

    public function addExport(Export $export): self
    {
        if (!$this->exports->contains($export)) {
            $this->exports[] = $export;
            $export->setScenario($this);
        }

        return $this;
    }

    public function removeExport(Export $export): self
    {
        if ($this->exports->contains($export)) {
            $this->exports->removeElement($export);
            // set the owning side to null (unless already changed)
            if ($export->getScenario() === $this) {
                $export->setScenario(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
