<?php

namespace App\Entity\Export;

class Upload
{

  private $file;

  public function setFile($file): self
  {
      $this->file = $file;
      return $this;
  }

  public function getFile()
  {
    return $this->file;
  }


}
