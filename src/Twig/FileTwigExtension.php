<?php

namespace App\Twig;

class FileTwigExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('fileIcon', array($this, 'getIcon'), array('is_safe' => ['html'])),
        );
    }

    public function getName()
    {
        return 'file_twig_extension';
    }

    public function getIcon($extension)
    {
        switch (strtolower($extension)) {
            case 'pdf': return '<i class="fa fa-lg fa-file-pdf-o"></i>';
            case 'xls': return "<i class=\"fa fa-lg fa-file-excel-o\"></i>&nbsp;";
            case 'xlsx': return "<i class=\"fa fa-lg fa-file-excel-o\"></i>&nbsp;";
            case 'ods': return "<i class=\"fa fa-lg fa-file-excel-o\"></i>&nbsp;";
            case 'txt': return "<i class=\"fa fa-lg fa-file-text-o\"></i>&nbsp;";
            case 'doc': return "<i class=\"fa fa-lg fa-file-word-o\"></i>&nbsp;";
            case 'docx': return "<i class=\"fa fa-lg fa-file-word-o\"></i>&nbsp;";
            case 'odt': return "<i class=\"fa fa-lg fa-file-word-o\"></i>&nbsp;";
            case 'ppt': return "<i class=\"fa fa-lg fa-file-powerpoint-o\"></i>&nbsp;";
            case 'odp': return "<i class=\"fa fa-lg fa-file-powerpoint-o\"></i>&nbsp;";
            case 'jpg': return "<i class=\"fa fa-lg fa-file-image-o\"></i>&nbsp;";
            case 'jpeg': return "<i class=\"fa fa-lg fa-file-image-o\"></i>&nbsp;";
            case 'png': return "<i class=\"fa fa-lg fa-file-image-o\"></i>&nbsp;";
            case 'gif': return "<i class=\"fa fa-lg fa-file-image-o\"></i>&nbsp;";
            case 'bmp': return "<i class=\"fa fa-lg fa-file-image-o\"></i>&nbsp;";
            case 'zip': return "<i class=\"fa fa-lg fa-file-zip-o\"></i>&nbsp;";
            default   : return "<i class=\"fa fa-lg fa-file-o\"></i>&nbsp;";
        }
    }

}